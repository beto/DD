# DD

[TOC]

## Català

DD és el workspace educatiu generat en el marc del Pla de Digitalització Democràtica d'Xnet. Ha estat creat i powered per
Xnet, famílies i centres promotors, IsardVDI, 3iPunt, MaadiX, eXO.cat, Evilham i financiat per la Direcció d'Innovació
Democràtica, Comissionat d'Innovació Digital, Comissionat d'Economia Social de l'Ajuntament de Barcelona, en
col·laboració amb Consorci d'Educació de Barcelona, aFFaC i AirVPN.

L'aplicació DD pot utilitzar-se lliurement sempre i quan consti aquest footer i es respecti la llicència AGPLv3
(https://www.gnu.org/licenses/agpl-3.0.en.html).

Trobareu meś informació en català a la documentació: [https://dd.digitalitzacio-democratica.xnet-x.net/docs/index.ca/](https://dd.digitalitzacio-democratica.xnet-x.net/docs/index.ca/).

Agraïm l'ajuda de Miriam Carles, Cristian Ruiz, Anna Francàs, Christopher Millard.

## Castellano

DD es el workspace educativo generado en el marco del Plan de Digitalización Democrática de Xnet. Ha sido creado y powered por
Xnet, familias y centros promotores, IsardVDI, 3ipunt, MaadiX, eXO.cat, Evilham y financiado por la Dirección de Innovación
Democrática, Comisionado de Innovación Digital, Comisionado de Economía Social del Ayuntamiento de Barcelona, en
colaboración con el Consorcio de Educación de Barcelona, aFFaC y AirVPN.

La aplicación DD puede utilizarse libremente siempre y cuando conste este footer y se respete la licencia AGPLv3
(https://www.gnu.org/licenses/agpl-3.0.en.html).

Más información en castellano en la documentación: [https://dd.digitalitzacio-democratica.xnet-x.net/docs/index.es/](https://dd.digitalitzacio-democratica.xnet-x.net/docs/index.es/).

Agradecemos la ayuda de Miriam Carles, Cristian Ruiz, Anna Francàs, Christopher Millard.


## English

DD is the education workspace generated within the framework of Xnet's Democratic Digitalisation Plan. It has been created and powered by
Xnet, families and promoting centres, IsardVDI, 3iPunt, MaadiX, eXO.cat, Evilham and funded by the Directorate for
Democratic Innovation, the Barcelona City Council's Digital Innovation Commissioner, Social Economy Commissioner, in
collaboration with the Barcelona Education Consortium, aFFaC and AirVPN.

DD can be used freely as long as this footer is included and the AGPLv3 license (https://www.gnu.org/licenses/agpl-3.0.en.html) is respected.

More info in English in the documentation: [https://dd.digitalitzacio-democratica.xnet-x.net/docs/](https://dd.digitalitzacio-democratica.xnet-x.net/docs/).

We thank the help of Miriam Carles, Cristian Ruiz, Anna Francàs, Christopher Millard.

# What is DD?

DD sets up an identity provider and many apps providing a cohesive user
experience considering schools and universities as the main use-case.

The project provides an integrated solution to handle the common
environment in education:

- **Classrooms**: A Moodle instance with custom theme and custom plugins
- **Cloud drive**: A Nextcloud instance with custom theme and custom plugins
- **Documents**: A document viewer and editor integrated with Nextcloud
- **Web pages**: A Wordpress instance with custom theme and custom plugins
- **Pad**: An Etherpad instance integrated with Nextcloud
- **Conferences**: BigBlueButton integrated with Moodle and Nextcloud (needs a standalone host)
- **Forms**: A forms Nextcloud plugin

|                              |                                 |
| ---------------------------- | ------------------------------- |
| ![](docs/img/classrooms.png) | ![](docs/img/cloud_storage.png) |

## Administration interface

The project includes an administration interface that allows to easily manage
users and groups and keep these in sync between applications.

| ![](docs/img/admin_sync.png) | ![](docs/img/admin_user_edit.png) |
| ---------------------------- | --------------------------------- |

To easily migrate and insert users and groups to the system there are also two
provided imports:

- From Google Admin Console as a JSON dump
- From a CSV file

# I'm interested!

That's great! Whether you want to contribute or are interested in deploying DD
for your organisation, we'll be happy to hear from you, here are some
resources to aid you further:

- **User handbook**: [https://dd.digitalitzacio-democratica.xnet-x.net/manual-usuari/](https://dd.digitalitzacio-democratica.xnet-x.net/manual-usuari/)
- **Admin/developer docs**: [https://dd.digitalitzacio-democratica.xnet-x.net/docs/](https://dd.digitalitzacio-democratica.xnet-x.net/docs/)
- **Source code**: [https://gitlab.com/DD-workspace/DD](https://gitlab.com/DD-workspace/DD)
