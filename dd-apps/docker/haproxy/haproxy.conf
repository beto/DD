#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
resolvers mydns
    nameserver dns1 127.0.0.11:53
    
global
#   debug
    daemon
    log             127.0.0.1    local0
    tune.ssl.default-dh-param 2048

  defaults
    mode http
    timeout         connect 120s
    timeout         client 120s
    timeout         client-fin 120s
    timeout         server 120s
    timeout         tunnel 7200s
    option          http-server-close
    option          httpclose
    log             global
    option          httplog
    backlog         4096
    maxconn         2000
    option          tcpka

frontend website
    mode http
    bind :80
    redirect scheme https if !{ ssl_fc }
    bind :443 ssl crt /certs/chain.pem

    acl is_nextcloud hdr_beg(host) nextcloud.
    acl is_moodle hdr_beg(host) moodle.
    acl is_jitsi hdr_beg(host) jitsi.

    use_backend be_nextcloud if is_nextcloud
    use_backend be_moodle if is_moodle
    use_backend be_jitsi if is_jitsi
    
    default_backend be_moodle

backend be_moodle
	mode http
  acl existing-x-forwarded-host req.hdr(X-Forwarded-Host) -m found
  acl existing-x-forwarded-proto req.hdr(X-Forwarded-Proto) -m found
  http-request add-header X-Forwarded-Host %[req.hdr(Host)] unless existing-x-forwarded-host
  http-request add-header X-Forwarded-Proto https unless existing-x-forwarded-proto
	server moodle moodle:8080 check port 8080 inter 5s rise 2 fall 10 resolvers mydns init-addr none

backend be_nextcloud
	mode http
  acl existing-x-forwarded-host req.hdr(X-Forwarded-Host) -m found
  acl existing-x-forwarded-proto req.hdr(X-Forwarded-Proto) -m found
  http-request add-header X-Forwarded-Host %[req.hdr(Host)] unless existing-x-forwarded-host
  http-request add-header X-Forwarded-Proto https unless existing-x-forwarded-proto
	server nextcloud nextcloud:80 check port 80 inter 5s rise 2 fall 10 resolvers mydns init-addr none

backend be_jitsi
	mode http
  acl existing-x-forwarded-host req.hdr(X-Forwarded-Host) -m found
  acl existing-x-forwarded-proto req.hdr(X-Forwarded-Proto) -m found
  http-request add-header X-Forwarded-Host %[req.hdr(Host)] unless existing-x-forwarded-host
  http-request add-header X-Forwarded-Proto https unless existing-x-forwarded-proto
	server jitsi jitsi:80 check port 80 inter 5s rise 2 fall 10 resolvers mydns init-addr none

  listen stats 
        bind                0.0.0.0:8888
        mode                http
        stats               enable
        option              httplog
        stats               show-legends
        stats               uri /haproxy
        stats               realm Haproxy\ Statistics
        stats               refresh 5s
        #stats               auth staging:pep1n1ll0
        #acl authorized http_auth(AuthUsers)
        #stats          http-request auth unless authorized
        timeout             connect 5000ms
        timeout             client 50000ms
        timeout             server 50000ms

userlist AuthUsers
    user admin password $6$grgQMVfwI0XSGAQl$2usaQC9LVXXXYHtSkGUf74CIGsiH8fi/K.V6DuKSq0twPkmFGP2vL/b//Ulp2I4xBEZ3eYDhUbwBPK8jpmsbo.
