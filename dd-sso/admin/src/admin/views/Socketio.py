#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# from flask_socketio import SocketIO, emit, join_room, leave_room, \
#     close_room, rooms, disconnect, send
# from admin import app
# import json

# # socketio = SocketIO(app)
# # from ...start import socketio

# @socketio.on('connect', namespace='//sio')
# def socketio_connect():
#     join_room('admin')
#     socketio.emit('update',
#                     json.dumps('Joined'),
#                     namespace='//sio',
#                     room='admin')

# @socketio.on('disconnect', namespace='//sio')
# def socketio_domains_disconnect():
#     None
