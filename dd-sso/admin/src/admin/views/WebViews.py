#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import concurrent.futures
import json
import logging as log
import os
import sys
import time
import traceback
from pprint import pprint
from uuid import uuid4

import requests
from flask import (
    Response,
    jsonify,
    redirect,
    request,
    send_file,
    url_for,
)
from flask import render_template as render_template_flask
from flask_login import login_required

from admin import app

from ..lib.avatars import Avatars
from .decorators import is_admin

avatars = Avatars()

from ..lib.legal import gen_legal_if_not_exists

"""  OIDC TESTS """
# from ..auth.authentication import oidc

# @app.route('/custom_callback')
# @oidc.custom_callback
# def callback(data):
#     return 'Hello. You submitted %s' % data

# @app.route('/private')
# @oidc.require_login
# def hello_me():
#     info = oidc.user_getinfo(['email', 'openid_id'])
#     return ('Hello, %s (%s)! <a href="/">Return</a>' %
#             (info.get('email'), info.get('openid_id')))


# @app.route('/api')
# @oidc.accept_token(True, ['openid'])
# def hello_api():
#     return json.dumps({'hello': 'Welcome %s' % g.oidc_token_info['sub']})


# @app.route('/logout')
# def logoutoidc():
#     oidc.logout()
#     return 'Hi, you have been logged out! <a href="/">Return</a>'
"""  OIDC TESTS """

def render_template(*args, **kwargs):
    kwargs["DOMAIN"] = os.environ["DOMAIN"]
    return render_template_flask(*args, **kwargs)

@app.route("/users")
@login_required
def web_users():
    return render_template("pages/users.html", title="Users", nav="Users")


@app.route("/roles")
@login_required
def web_roles():
    return render_template("pages/roles.html", title="Roles", nav="Roles")


@app.route("/groups")
@login_required
def web_groups(provider=False):
    return render_template("pages/groups.html", title="Groups", nav="Groups")


@app.route("/avatar/<userid>", methods=["GET"])
@login_required
def avatar(userid):
    if userid != "false":
        return send_file("../avatars/master-avatars/" + userid, mimetype="image/jpeg")
    return send_file("static/img/missing.jpg", mimetype="image/jpeg")


@app.route("/dashboard")
@login_required
def dashboard(provider=False):
    data = json.loads(requests.get("http://dd-sso-api/json").text)
    return render_template(
        "pages/dashboard.html", title="Customization", nav="Customization", data=data
    )


@app.route("/legal")
@login_required
def legal():
    # data = json.loads(requests.get("http://dd-sso-api/json").text)
    return render_template("pages/legal.html", title="Legal", nav="Legal", data={})

@app.route("/legal_text")
def legal_text():
    lang = request.args.get("lang")
    if not lang or lang not in ["ca","es","en","fr"]:
        lang="ca"
    gen_legal_if_not_exists(lang)
    return render_template("pages/legal/"+lang)

### SYS ADMIN


@app.route("/sysadmin/users")
@login_required
@is_admin
def web_sysadmin_users():
    return render_template(
        "pages/sysadmin/users.html", title="SysAdmin Users", nav="SysAdminUsers"
    )


@app.route("/sysadmin/groups")
@login_required
@is_admin
def web_sysadmin_groups():
    return render_template(
        "pages/sysadmin/groups.html", title="SysAdmin Groups", nav="SysAdminGroups"
    )


@app.route("/sysadmin/external")
@login_required
## SysAdmin role
def web_sysadmin_external():
    return render_template(
        "pages/sysadmin/external.html", title="External", nav="External"
    )


@app.route("/sockettest")
def web_sockettest():
    return render_template(
        "pages/sockettest.html", title="Sockettest Users", nav="SysAdminUsers"
    )
